import url from './url';

const Get = (path, subPath) => {
  const promise = new Promise((resolve, reject) => {
    url.get(`/${path}${subPath ? subPath : ''}`)
    .then(({ data }) => {
      resolve(data)
    },(err) => {
      reject(err)
    })
  })
  return promise
}

const getPost = (subPath) => Get('posts', subPath);

const api = {
  getPost,
}

export default api;