import axios from 'axios';

const cors = `https://cors-anywhere.herokuapp.com/`

const instance = axios.create({
  baseURL: `${cors}https://id.techinasia.com/wp-json/techinasia/3.0/`
});

export default instance;