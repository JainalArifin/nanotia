import React, { Component } from "react"
import ListPost from "../components/ListPost/ListPost";
import SEO from "../components/seo";
import Layout from '../components/layout'
// redux
import { connect } from 'react-redux';
import { fetchPostData, fetchPostDataScroll } from '../redux/actions/index'
import Loader from "../components/Loader/Loader";
import InfiniteScroll from "react-infinite-scroll-component";

import '../assets/styles/index.css';
class IndexPage extends Component {
  state = {
    size: 5,
  }

  getData = () => {
    const { size } = this.state
    this.props.fetchDataScroll(`?per_page=${size}`)
  }

  componentDidMount() {
    this.props.fetchData('?per_page=5')
  }


  // ==== infinte scroll ====
  fetMoreDataNew = () => {
    this.setState({
      size: this.state.size + 1
    }, this.getData)
  }
  // ===== infinte scroll end ===
  render() {
    return (
      <Layout>
        <SEO
          image="https://instanid-insight.com/wp-content/uploads/2018/10/5948c851e6a1f54b184b2412_https3A2F2Fcdn.techinasia.com2Fdata2Fimages2F851a67f98c6a05d65ba06645f55c090b.png"
          title="Tech in Asia Indonesia - Komunitas Online Startup di Asia"
          description="Tech in Asia adalah komunitas online pelaku startup di Asia. Temukan investor, founder, dan berita menarik seputar Asia di sini."
        />
        <div>
          {this.props.isLoad ? (
            <div className="wrap-app d-flex align-items-center">
              <Loader />
            </div>
          ) : (
              <InfiniteScroll
                dataLength={this.props.data.posts && this.props.data.posts.length}
                next={this.fetMoreDataNew}
                hasMore={this.state.size}
                loader={<div className="text-center">Loading...</div>}
              >
                <div className="container wrap-post-list mt-3">
                  {this.props.data.posts && this.props.data.posts.map((list, key) => (
                    <ListPost
                      title={list.title}
                      excerpt={list.excerpt}
                      ga_type={list.ga_type}
                      imagePost={list.seo.image}
                      author={list.author}
                      date={list.date}
                      slug={list.slug}
                      key={key} />
                  ))}
                </div>
              </InfiniteScroll>
            )}
        </div>
      </Layout>
    )
  }
}

const mapStateToProps = state => {
  return {
    data: state.data.dataPost,
    isLoad: state.data.isLoad
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchData: (subPatch) => dispatch(fetchPostData(subPatch)),
    fetchDataScroll: (subPatch) => dispatch(fetchPostDataScroll(subPatch))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IndexPage);