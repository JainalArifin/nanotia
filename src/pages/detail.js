import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchPostDetail } from '../redux/actions'
import moment from 'moment';
import Layout from '../components/layout';

import '../assets/styles/detail.css';
import SEO from '../components/seo';
import Loader from '../components/Loader/Loader';

class Detail extends Component {
  componentDidMount() {
    this.props.getDetail(`/${this.props.location.search.split("=")[1]}`)
  }
  render() {
    // const { ga_type, title, author, content } = this.props.detailPost.posts[0]
    return (
      <Layout>
        {this.props.isLoad ?
          <div className="wrap-app d-flex align-items-center">
            <Loader />
          </div>
          :
          <div className="container p-0 wrap-post-list mt-5 mb-5">
            {this.props.detailPost.posts !== undefined && this.props.detailPost.posts.map((list, key) => (
              <div key={key}>
                {/* start of seo */}
                <SEO
                  description={list.seo.description}
                  image={list.seo.image}
                  title={list.seo.title}
                />
                {/* end of seo */}

                <div className="p-5">
                  <p className="ga_type-list">{list.ga_type}</p>
                  <h1 dangerouslySetInnerHTML={{ __html: list.title }} />
                  {/* start of author */}
                  <div className="d-flex mt-3">
                    <img src={list.author.avatar_url} alt={list.author.display_name} className="avatar" />
                    <div>
                      <div>{list.author.display_name}</div>
                      <div>{moment(list.date).format('LLL')}</div>
                    </div>
                  </div>
                  {/* end of author */}
                </div>
                <img src={list.seo.image} alt={list.title}
                  className="img-post-detail"
                />
                <div
                  className="p-5"
                  dangerouslySetInnerHTML={{ __html: list.content }}
                />
              </div>
            ))}
          </div>
        }
      </Layout>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    detailPost: state.data.detail,
    isLoad: state.data.isLoad
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getDetail: (slug) => dispatch(fetchPostDetail(slug))
  }
}

export default connect(
  mapStateToProps, mapDispatchToProps
)(Detail);