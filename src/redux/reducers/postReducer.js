// githubReducer.js
const iniTialState = {
  dataPost: [],
  detail :{},
  isLoad: false
}

export default function githubReducer(state = iniTialState, action) {
  switch (action.type) {
    case 'FETCH_POST_DATA':
      return {...state, dataPost:action.data};
    case 'FETCH_POST_DETAIL':
      return {...state, detail:action.data};
    case 'IS_LOAD':
      return {...state, isLoad: action.data}
    default:
      return state;
  }
}