// index.js

import { FETCH_POST_DATA, FETCH_POST_DETAIL, IS_LOAD } from './types';
import api from '../../config/request'


export const fetchData = (data) => {
  return {
    type: FETCH_POST_DATA,
    data
  }
};

export const fetchPostData = (subPath) => {
  return (dispatch) => {
    dispatch({ type: IS_LOAD, data: true })
    return api.getPost(subPath)
      .then(data => {
        dispatch({ type: IS_LOAD, data: false })
        dispatch(fetchData(data))
      })
      .catch(error => {
        throw (error);
      });
  };
};
export const fetchPostDataScroll = (subPath) => {
  return (dispatch) => {
    return api.getPost(subPath)
      .then(data => {
        dispatch(fetchData(data))
      })
      .catch(error => {
        throw (error);
      });
  };
};



export const fetchDataDetail = (data) => {
  return {
    type: FETCH_POST_DETAIL,
    data
  }
};

export const fetchPostDetail = (slug) => {
  return (dispatch) => {
    dispatch({ type: IS_LOAD, data: true })
    return api.getPost(slug)
      .then(data => {
        dispatch({ type: IS_LOAD, data: false })
        dispatch(fetchDataDetail(data))
      })
      .catch(error => {
        throw (error);
      });
  };
};