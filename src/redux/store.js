import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

import rootReducer from './reducers';
// import { fetchPostData } from './actions/index';

const store = createStore(rootReducer, applyMiddleware(logger, thunk));

// store.dispatch(fetchPostData());

export default store;