import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import Logo from '../images/logo.png'

const Header = ({ siteTitle }) => (
  <header
    className="p-2 shadow"
  >
    <div className="container">
        <Link
          to="/"
          style={{
            color: `white`,
            textDecoration: `none`,
          }}
        >
          {/* {siteTitle} */}
          <img src={Logo} alt="logo" 
            style={{
              width:'172px',
              height: '32px'
            }}
          />
        </Link>
    </div>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
