import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Link } from 'gatsby'

import './listpost.css';


const ListPost = ({ 
  title, slug, excerpt, ga_type, 
  author, date, imagePost
}) => (
  <div className="row list-item">
    <div className="col-md-8">
      <div className="ga_type-list">{ga_type}</div>
      <Link  
        style={{ textDecoration: 'none' }}
        to={`/detail/?slug=${slug}`}
        >
        <div 
          className="title-post"
          dangerouslySetInnerHTML={{__html: title}} 
        />
      </Link>
      <p 
        className="excerpt-post"
        dangerouslySetInnerHTML={{__html: excerpt}} 
      />
      <div className="d-flex">
        <img src={author.avatar_url} alt={author.display_name} className="avatar" />
        <div>
          <div>{author.display_name}</div>
          <div>{moment(date, "YYYYMMDD").fromNow()}</div>
        </div>
      </div>

    </div>
    <div className="col-md-4">
      <Link to={`/detail/?slug=${slug}`}>
        <img src={imagePost} className="img-post" alt="presentation" />
      </Link>
    </div>
  </div>
);

ListPost.propTypes = {
  title: PropTypes.string,
  slug: PropTypes.string,
  excerpt: PropTypes.string,
  imagePost: PropTypes.string,
  ga_type: PropTypes.string,
  author: PropTypes.object,
  date: PropTypes.string,
};

ListPost.defaultProps = {
  title: '',
  slug: '',
  excerpt: '',
  imagePost: '',
  ga_type: '',
  author: {},
  date: ''

}


export default ListPost;